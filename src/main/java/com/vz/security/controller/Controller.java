package com.vz.security.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.vz.security.model.User;
import com.vz.security.repository.RedisRepositoryImpl;
import com.vz.security.utils.Util;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/user")
public class Controller {	
	@Autowired
	RedisRepositoryImpl redisRepositoryImpl;
	
	@Autowired
	Util util;
	
	private static String USER = "USER";
	
	@RequestMapping(value = "/create-user", method = RequestMethod.POST, produces = "application/json")	
	@ApiOperation(value = "Creates the bridge with controller specific information ", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Created"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody Map<String,String> addBridge(@RequestBody User user){		
		Map<String,String> message = new HashMap<String,String>();	
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date)); 
		user.setCreatDate(dateFormat.format(date));
		
		String task = util.encrypt(user.getPassword());
		
		System.out.println(task);
		user.setPassword(task);
		redisRepositoryImpl.saveInstances(USER, user.getUserName(), user);
		System.out.println("save User Done!");
		return message;
	}
	
	@RequestMapping(value = "/getAllUser", method = RequestMethod.GET, produces = "application/json")	
	@ApiOperation(value = "Creates the bridge with controller specific information ", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Created"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody List<Object> getAllUser(){				
		System.out.println("save User Done!");
		return redisRepositoryImpl.getAll(USER);
	}
	
	@RequestMapping(value = "{username}/deleteUser", method = RequestMethod.DELETE, produces = "application/json")	
	@ApiOperation(value = "Creates the bridge with controller specific information ", notes = "Returns success or failure SLA:500")
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successful Created"),
			@ApiResponse(code = 400, message = "Invalid input provided"),
			@ApiResponse(code = 404, message = "given Transaction ID does not exist"), })
	public @ResponseBody Map<String,String> deleteUser(@PathVariable String username){				
		System.out.println("User deleted!");
		Map<String,String> message = new HashMap<String,String>();
		Long x = redisRepositoryImpl.deleteInstances(USER, username);
		System.out.println("Deleted! "+x);
		message.put("type", "sucess");
		message.put("message", "deleted sucessfully "+x);
		return message;
	}
	
	
	
	
	
}