package com.vz.security.repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.HashOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;

@Repository
public class RedisRepositoryImpl implements RedisRepository {

	private RedisTemplate<String, Object> redisTemplate;
	private HashOperations<String,String,Object> hashOps;
	private ValueOperations<String, Object> TokenOps;

	@Autowired
	public RedisRepositoryImpl(RedisTemplate<String, Object> redisTemplate) {
		this.redisTemplate = redisTemplate;
	}

	@PostConstruct
	private void init() {
		hashOps = redisTemplate.opsForHash();
		TokenOps = redisTemplate.opsForValue();
	}

	@Override
	public void saveInstances(String key, String id, Object object) {
		hashOps.put(key, id, object);

	}

	@Override
	public void updateInstances(String key, String id, Object object) {
		hashOps.put(key, id, object);

	}

	@Override
	public Object findInstances(String key, String id) {

		return hashOps.get(key, id);
	}

	@Override
	public Map<String, Object> findAllInstances(String key) {

		return hashOps.entries(key);
	}
	
	@Override
	public List<Object> getAll(String key){
		List<Object> listObject = new ArrayList<Object>();
		listObject = hashOps.values(key);
		return listObject; 
	}

	@Override
	public Long deleteInstances(String key, String id) {

		return hashOps.delete(key, id);
	}
	
	@Override
	public Object getLoginInfo(String key) {
		return TokenOps.get(key);
	}
	public void save(String key,String value) {
		TokenOps.set(key, value);
	}
	
	public String get(String key, String id){
		return (String) hashOps.get(key, id);
	}
	
}
