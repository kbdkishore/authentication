package com.vz.security.repository;

import java.util.List;
import java.util.Map;

public interface  RedisRepository {
	
	public void saveInstances(String key,String id, Object object);
	
	public void updateInstances(String key,String id, Object object);
	public Object getLoginInfo(String key);	
	public Object findInstances(String key,String id);
	
	public Map<String, Object> findAllInstances(String key);
	
	public Long deleteInstances(String key,String id);
	
	public List<Object> getAll(String key);
	

}
